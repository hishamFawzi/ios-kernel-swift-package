import XCTest

import iOSKernelTests

var tests = [XCTestCaseEntry]()
tests += iOSKernelTests.allTests()
XCTMain(tests)
