import XCTest
@testable import iOSKernel

final class iOSKernelTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(iOSKernel().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
