//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct Dataq: Codable {

    let response: Response?

    enum CodingKeys: String, CodingKey {
        case response
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        response = try values.decode(Response.self, forKey: .response)
    }

}
