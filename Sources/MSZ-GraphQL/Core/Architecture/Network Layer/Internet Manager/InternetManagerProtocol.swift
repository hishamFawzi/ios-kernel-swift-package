//
//  InternetManagerProtocol.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/13/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation

protocol InternetManagerProtocol: class {
    func  isInternetConnectionAvailable () -> Bool
}
