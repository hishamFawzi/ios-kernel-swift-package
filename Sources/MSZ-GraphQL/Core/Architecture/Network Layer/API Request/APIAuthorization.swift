//
//  APIAuthorization.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/13/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation

enum APIAuthorization {
    case none
    case bearerToken
    var authData: Any {
        switch self {
        case .none:
            return []
        case .bearerToken:
            let token  = ""
            return ["Authorization": "Bearer \(token)"]
        }
    }
}
