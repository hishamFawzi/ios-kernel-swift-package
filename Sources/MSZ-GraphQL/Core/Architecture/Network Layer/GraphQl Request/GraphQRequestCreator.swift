//
//  GraphQRequestCreator.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 3/10/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Apollo
import Foundation
extension BaseGraphQlRequest {
     func createRequest<Operation: GraphQLOperation>(for operation: Operation,
                                                     files: [GraphQLFile]?) throws -> URLRequest {
        let body = requestBody(for: operation)
        var request = URLRequest(url: self.url)
        switch authorization {
        case .bearerToken:
            guard let header = authorization.authData as? [String: String] else {
                fatalError("bearerToken header must be as [String:String] ")
            }
            headers.merge(header) { (_, new) in new }
        case .none:
            break
        }
        request.allHTTPHeaderFields = headers

        switch method {
        case .get:
            let transformer = GraphQLGETTransformer(body: body, url: self.url)
            if let urlForGet = transformer.createGetURL() {
                request = URLRequest(url: urlForGet)
                request.httpMethod = method.rawValue
            } else {
                throw GraphQLHTTPRequestError.serializedQueryParamsMessageError
            }
        case .post:
            do {
                if
                    let files = files,
                    !files.isEmpty {
                    let formData = try requestMultipartFormData( for: operation,
                                                                 files: files,
                                                                 serializationFormat: self.serializationFormat,
                                                                 manualBoundary: nil)

                    request.setValue("multipart/form-data; boundary=\(formData.boundary)", forHTTPHeaderField: "Content-Type")
                    request.httpBody = try formData.encode()
                } else {
                    request.httpBody = try serializationFormat.serialize(value: body)
                }

                request.httpMethod = method.rawValue
            } catch {
                throw GraphQLHTTPRequestError.serializedBodyMessageError
            }
        default:
            fatalError(" the \(method) method not allowed ")
        }

        request.setValue(operation.operationName, forHTTPHeaderField: "X-APOLLO-OPERATION-NAME")

        if let operationID = operation.operationIdentifier {
            request.setValue(operationID, forHTTPHeaderField: "X-APOLLO-OPERATION-ID")
        }

        return request
    }

    private func requestBody<Operation: GraphQLOperation>(for operation: Operation,
                                                          sendQueryDocument: Bool = true) -> GraphQLMap {
        var body: GraphQLMap = [
            "variables": operation.variables,
            "operationName": operation.operationName
        ]
        if sendQueryDocument {
            body["query"] = operation.queryDocument
        }
        return body
    }

    private func requestMultipartFormData<Operation: GraphQLOperation>(for operation: Operation,
                                                                       files: [GraphQLFile],
                                                                       serializationFormat: JSONSerializationFormat.Type,
                                                                       manualBoundary: String?) throws -> MultipartFormData {
        let formData: MultipartFormData

        if let boundary = manualBoundary {
            formData = MultipartFormData(boundary: boundary)
        } else {
            formData = MultipartFormData()
        }

        // Make sure all fields for files are set to null, or the server won't look
        // for the files in the rest of the form data
        let fieldsForFiles = Set(files.map { $0.fieldName })
        var fields = requestBody(for: operation)
        var variables = fields["variables"] as? GraphQLMap ?? GraphQLMap()
        for fieldName in fieldsForFiles {
            if
                let value = variables[fieldName],
                let arrayValue = value as? [JSONEncodable] {
                let updatedArray: [JSONEncodable?] = arrayValue.map { _ in nil }
                variables.updateValue(updatedArray, forKey: fieldName)
            } else {
                variables.updateValue(nil, forKey: fieldName)
            }
        }
        fields["variables"] = variables

        let operationData = try serializationFormat.serialize(value: fields)
        formData.appendPart(data: operationData, name: "operations")

        var map = [String: [String]]()
        if files.count == 1 {
            let firstFile = files.first!
            map["0"] = ["variables.\(firstFile.fieldName)"]
        } else {
            for (index, file) in files.enumerated() {
                map["\(index)"] = ["variables.\(file.fieldName).\(index)"]
            }
        }

        let mapData = try serializationFormat.serialize(value: map)
        formData.appendPart(data: mapData, name: "map")

        for (index, file) in files.enumerated() {
            formData.appendPart(inputStream: file.inputStream,
                                contentLength: file.contentLength,
                                name: "\(index)",
                contentType: file.mimeType,
                filename: file.originalName)
        }

        return formData
    }
}
