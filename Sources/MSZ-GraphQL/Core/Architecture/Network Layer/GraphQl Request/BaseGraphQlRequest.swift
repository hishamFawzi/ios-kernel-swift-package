//
//  BaseGraphQlRequest.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 3/9/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation
import Apollo

class BaseGraphQlRequest<Operation: GraphQLOperation>: APIRequestProtocol {

    var queryParamss: [String: String]?
    var scheme: String
    var baseDomain: String
    var portNumber: Int
    var path: String
    var authorization: APIAuthorization
    var method: HTTPMethod
    var queryBody: Any?
    var headers: [String: String]
    var queryItems: [URLQueryItem]?
    var url: URL {
        var urlComponents = URLComponents()
        urlComponents.scheme = self.scheme
        urlComponents.port = self.portNumber
        urlComponents.path = "\(self.path)"
        urlComponents.host = self.baseDomain
        return urlComponents.url!
    }
    var operation: Operation!

    var requestURL: URLRequest {
        do {
            return try createRequest(for: operation, files: nil)
        } catch {
            #if DEBUG
            fatalError(error.localizedDescription)
            #else
            return URLRequest(url: url)
            #endif
        }
    }

    let serializationFormat = JSONSerializationFormat.self

    init() {
        portNumber = Environment().configuration(.port).integerValue
        scheme = "\(Environment().configuration(.urlProtocol))"
        baseDomain = "\(Environment().configuration(.baseDomain))"
        authorization = .none
        headers = ["Content-Type": "application/json"]
        path = "/graphql"
        method = .post
    }
    func queryParams() -> [String: String]? {
        return nil
    }

}
