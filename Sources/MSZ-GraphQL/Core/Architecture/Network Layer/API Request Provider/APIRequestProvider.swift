//
//  APIRequestProvider.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/13/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation
import Alamofire

class APIRequestProvider: NSObject, APIRequestProviderProtocol {

    @Injected var internetManager: InternetManagerProtocol

    func perform(apiRequest: APIRequestProtocol, completion: @escaping APIRequestCompletion) {
        guard internetManager.isInternetConnectionAvailable() else {
            completion(Swift.Result<Data, APIRequestProviderError>.failure(.noInternet(message: "NoInterneError".localized)))
            return
        }
        performRequest(apiRequest.requestURL, completion: completion)
    }

    private func performRequest( _ request: URLRequest, completion: @escaping APIRequestCompletion) {

        Alamofire.request(request)
        .validate()
        .responseData(completionHandler: { (response) in
            let statusCode = response.response?.statusCode ?? 400
            switch response.result {
            case .success(let data):
                completion(Swift.Result<Data, APIRequestProviderError>.success(data))
            case .failure:
                let failure = APIRequestProviderError.server(statusCode: statusCode, data: response.data)
                completion(Swift.Result<Data, APIRequestProviderError>.failure(failure))
            }
        })
    }
}
