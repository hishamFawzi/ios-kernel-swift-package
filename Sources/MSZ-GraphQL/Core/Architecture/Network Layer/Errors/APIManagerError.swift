//
//  APIManagerError.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/13/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation

enum APIManagerError: Error {
    case requestFailed(message: String)
    case errorModel(errorModel: APIErrorModel)
    case noInternet(message: String)

    var message: String {
        switch self {
        case .requestFailed(let message):
            return message
        case .errorModel(let errorModel):
            return errorModel.errorDetail ?? ""
        case .noInternet(let message):
            return message
        }
    }
}
