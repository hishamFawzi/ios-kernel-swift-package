//
//  Resolver+ResolverRegistering.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/18/20.
//  Copyright © 2020 MSZ. All rights reserved.
//

import Foundation

extension Resolver: ResolverRegistering {

    public static func registerAllServices() {
        setupReposNetworkDependencies()
    }
}
