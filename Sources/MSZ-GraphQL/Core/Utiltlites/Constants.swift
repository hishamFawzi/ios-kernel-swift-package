//
//  Constants.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/13/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation

enum Constants {
    ///Radius in meters
    static let userRadius: Double = 500
}
