//
//  BaseVC.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/21/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation
import UIKit

protocol BaseViewProtocal: AnyObject {
    func showloader()
    func hideLoader()
    func showErrorMessage(message: String)
    func showAlertMessage(message: String)
}

class BaseVC<V, P>: UIViewController, BaseViewProtocal where P: BasePresenter<V> {

     @LazyInjected var presenter: P

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let view = self as? V else {
            fatalError("Presenter of type \(P.Type.self) need to attach View of type \(V.Type.self)")
        }
        presenter.attach(view: view)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter.viewWillDisappear()

    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter.viewDidDisappear()
    }

    func showloader() {

    }

    func hideLoader() {

    }

    func showErrorMessage(message: String) {

    }

    func showAlertMessage(message: String) {

    }

}
