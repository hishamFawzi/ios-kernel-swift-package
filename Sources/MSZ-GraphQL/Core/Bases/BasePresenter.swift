//
//  BasePresenter.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/21/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation
import UIKit
import Promises

class BasePresenter<T> {
    @Injected var network: NetworkManagerProtocol
    public var providers: [APIRequestProviderProtocol] = [Resolver.resolve()]
    weak private var  viewHolder: AnyObject!
    lazy var view: T! = {
        guard let view = viewHolder as? T else {
            fatalError("View of type \(T.Type.self) not attaced Yet")
        }
        return view
    }()

    func attach(view: T) {
        self.viewHolder =  view as AnyObject
        viewDidAttach()
    }

    open func viewDidAttach() { }
    open func viewWillDisappear() { }
    open func viewDidDisappear() { }
    open func viewWillAppear() { }
    open func viewDidAppear() { }
    open func perform<T: Codable>(apiRequest: APIRequestProtocol,
                                  providerType: APIRequestProviderProtocol? = nil,
                                  outputType: T.Type) -> Promise<T> {
        let providerType = providerType ?? providers[0]
        return network.perform(apiRequest: apiRequest, providerType: providerType, outputType: outputType)
    }

}
