//
//  Language.swift
//  Fawry-TicketsMall
//
//  Created by Ali Hamed on 8/28/19.
//  Copyright © 2019 Robusta. All rights reserved.
//

import Foundation
import UIKit

// constants
let appleLanguageKey = "AppleLanguages"
/// L102Language
class Language {
    /// get current Apple language
    class func currentAppleLanguage() -> String {
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: appleLanguageKey) as! NSArray
        let current = langArray.firstObject as! String
        let reqIndex = current.index(current.startIndex, offsetBy: 2)
        let currentWithoutLocale = String(current[..<reqIndex])
        return currentWithoutLocale
    }

    class func currentAppleLanguageFull() -> String {
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: appleLanguageKey) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }

    /// set @lang to be the first in Applelanguages list
    class func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang, currentAppleLanguage()], forKey: appleLanguageKey)
        userdef.synchronize()
    }

    class var isRTL: Bool {
        return Language.currentAppleLanguage() == "ar"
    }

}
