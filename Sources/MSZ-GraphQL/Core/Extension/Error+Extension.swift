//
//  Error+Extension.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/18/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation
extension Error {
    var message: String {
        if let apiError = self as? APIManagerError {
            return apiError.message
        } else if let apiError = self as? APIRequestProviderError {
            return apiError.reseon
        } else {
            let error = self as NSError
             return error.domain
        }
    }
}
