//
//  Environment.swift
//  MSZ-GraphQL
//
//  Created by MSZ on 2/13/20.
//  Copyright © 2020 Robusta. All rights reserved.
//

import Foundation
public struct Environment {

    fileprivate var infoDict: [String: Any] {
            if let dict = Bundle.main.infoDictionary {
                return dict
            } else {
                fatalError("Plist file not found")
            }
    }
    public func configuration(_ key: PlistKey) -> NSString {
        guard let keyValue = infoDict[key.value()] as? NSString else {
            fatalError("Key \(key.value()) Not founded")
        }
        return keyValue
    }
}
