//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public struct CustomerInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(dateOfBirth: Swift.Optional<String?> = nil, dob: Swift.Optional<String?> = nil, email: Swift.Optional<String?> = nil, firstname: Swift.Optional<String?> = nil, gender: Swift.Optional<Int?> = nil, isSubscribed: Swift.Optional<Bool?> = nil, lastname: Swift.Optional<String?> = nil, middlename: Swift.Optional<String?> = nil, password: Swift.Optional<String?> = nil, `prefix`: Swift.Optional<String?> = nil, suffix: Swift.Optional<String?> = nil, taxvat: Swift.Optional<String?> = nil) {
    graphQLMap = ["date_of_birth": dateOfBirth, "dob": dob, "email": email, "firstname": firstname, "gender": gender, "is_subscribed": isSubscribed, "lastname": lastname, "middlename": middlename, "password": password, "prefix": `prefix`, "suffix": suffix, "taxvat": taxvat]
  }

  /// The customer's date of birth
  public var dateOfBirth: Swift.Optional<String?> {
    get {
      return graphQLMap["date_of_birth"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "date_of_birth")
    }
  }

  /// Deprecated: Use `date_of_birth` instead
  public var dob: Swift.Optional<String?> {
    get {
      return graphQLMap["dob"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "dob")
    }
  }

  /// The customer's email address. Required for customer creation
  public var email: Swift.Optional<String?> {
    get {
      return graphQLMap["email"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  /// The customer's first name
  public var firstname: Swift.Optional<String?> {
    get {
      return graphQLMap["firstname"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "firstname")
    }
  }

  /// The customer's gender (Male - 1, Female - 2)
  public var gender: Swift.Optional<Int?> {
    get {
      return graphQLMap["gender"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gender")
    }
  }

  /// Indicates whether the customer is subscribed to the company's newsletter
  public var isSubscribed: Swift.Optional<Bool?> {
    get {
      return graphQLMap["is_subscribed"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "is_subscribed")
    }
  }

  /// The customer's family name
  public var lastname: Swift.Optional<String?> {
    get {
      return graphQLMap["lastname"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lastname")
    }
  }

  /// The customer's middle name
  public var middlename: Swift.Optional<String?> {
    get {
      return graphQLMap["middlename"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "middlename")
    }
  }

  /// The customer's password
  public var password: Swift.Optional<String?> {
    get {
      return graphQLMap["password"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "password")
    }
  }

  /// An honorific, such as Dr., Mr., or Mrs.
  public var `prefix`: Swift.Optional<String?> {
    get {
      return graphQLMap["prefix"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "prefix")
    }
  }

  /// A value such as Sr., Jr., or III
  public var suffix: Swift.Optional<String?> {
    get {
      return graphQLMap["suffix"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "suffix")
    }
  }

  /// The customer's Tax/VAT number (for corporate customers)
  public var taxvat: Swift.Optional<String?> {
    get {
      return graphQLMap["taxvat"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "taxvat")
    }
  }
}

public final class LoginMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    mutation Login($email: String!, $password: String!) {
      response: generateCustomerToken(email: $email, password: $password) {
        __typename
        token
      }
    }
    """

  public let operationName = "Login"

  public var email: String
  public var password: String

  public init(email: String, password: String) {
    self.email = email
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["email": email, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("generateCustomerToken", alias: "response", arguments: ["email": GraphQLVariable("email"), "password": GraphQLVariable("password")], type: .object(Response.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(response: Response? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "response": response.flatMap { (value: Response) -> ResultMap in value.resultMap }])
    }

    /// Retrieve the customer token
    public var response: Response? {
      get {
        return (resultMap["response"] as? ResultMap).flatMap { Response(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "response")
      }
    }

    public struct Response: GraphQLSelectionSet {
      public static let possibleTypes = ["CustomerToken"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("token", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(token: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "CustomerToken", "token": token])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// The customer token
      public var token: String? {
        get {
          return resultMap["token"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "token")
        }
      }
    }
  }
}

public final class RegisterMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    mutation Register($input: CustomerInput!) {
      response: createCustomer(input: $input) {
        __typename
        customer {
          __typename
          ...customerDetails
        }
      }
    }
    """

  public let operationName = "Register"

  public var queryDocument: String { return operationDefinition.appending(CustomerDetails.fragmentDefinition) }

  public var input: CustomerInput

  public init(input: CustomerInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createCustomer", alias: "response", arguments: ["input": GraphQLVariable("input")], type: .object(Response.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(response: Response? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "response": response.flatMap { (value: Response) -> ResultMap in value.resultMap }])
    }

    /// Create customer account
    public var response: Response? {
      get {
        return (resultMap["response"] as? ResultMap).flatMap { Response(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "response")
      }
    }

    public struct Response: GraphQLSelectionSet {
      public static let possibleTypes = ["CustomerOutput"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("customer", type: .nonNull(.object(Customer.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(customer: Customer) {
        self.init(unsafeResultMap: ["__typename": "CustomerOutput", "customer": customer.resultMap])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var customer: Customer {
        get {
          return Customer(unsafeResultMap: resultMap["customer"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "customer")
        }
      }

      public struct Customer: GraphQLSelectionSet {
        public static let possibleTypes = ["Customer"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(CustomerDetails.self),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: Int? = nil, firstname: String? = nil, lastname: String? = nil, email: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Customer", "id": id, "firstname": firstname, "lastname": lastname, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var fragments: Fragments {
          get {
            return Fragments(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public struct Fragments {
          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public var customerDetails: CustomerDetails {
            get {
              return CustomerDetails(unsafeResultMap: resultMap)
            }
            set {
              resultMap += newValue.resultMap
            }
          }
        }
      }
    }
  }
}

public final class IdentityQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    query Identity {
      customer {
        __typename
        ...customerDetails
        addresses {
          __typename
          id
          country_id
          city {
            __typename
            id
            name
            code
          }
          firstname
          lastname
          region {
            __typename
            region
            region_code
          }
          street
          company
          postcode
          telephone
        }
      }
    }
    """

  public let operationName = "Identity"

  public var queryDocument: String { return operationDefinition.appending(CustomerDetails.fragmentDefinition) }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("customer", type: .object(Customer.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(customer: Customer? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }])
    }

    /// The customer query returns information about a customer account
    public var customer: Customer? {
      get {
        return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "customer")
      }
    }

    public struct Customer: GraphQLSelectionSet {
      public static let possibleTypes = ["Customer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(CustomerDetails.self),
        GraphQLField("addresses", type: .list(.object(Address.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// An array containing the customer's shipping and billing addresses
      public var addresses: [Address?]? {
        get {
          return (resultMap["addresses"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Address?] in value.map { (value: ResultMap?) -> Address? in value.flatMap { (value: ResultMap) -> Address in Address(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Address?]) -> [ResultMap?] in value.map { (value: Address?) -> ResultMap? in value.flatMap { (value: Address) -> ResultMap in value.resultMap } } }, forKey: "addresses")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var customerDetails: CustomerDetails {
          get {
            return CustomerDetails(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }

      public struct Address: GraphQLSelectionSet {
        public static let possibleTypes = ["CustomerAddress"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(Int.self)),
          GraphQLField("country_id", type: .scalar(String.self)),
          GraphQLField("city", type: .object(City.selections)),
          GraphQLField("firstname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("region", type: .object(Region.selections)),
          GraphQLField("street", type: .list(.scalar(String.self))),
          GraphQLField("company", type: .scalar(String.self)),
          GraphQLField("postcode", type: .scalar(String.self)),
          GraphQLField("telephone", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: Int? = nil, countryId: String? = nil, city: City? = nil, firstname: String? = nil, lastname: String? = nil, region: Region? = nil, street: [String?]? = nil, company: String? = nil, postcode: String? = nil, telephone: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "CustomerAddress", "id": id, "country_id": countryId, "city": city.flatMap { (value: City) -> ResultMap in value.resultMap }, "firstname": firstname, "lastname": lastname, "region": region.flatMap { (value: Region) -> ResultMap in value.resultMap }, "street": street, "company": company, "postcode": postcode, "telephone": telephone])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// The ID assigned to the address object
        public var id: Int? {
          get {
            return resultMap["id"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        /// The customer's country
        @available(*, deprecated, message: "Use `country_code` instead.")
        public var countryId: String? {
          get {
            return resultMap["country_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "country_id")
          }
        }

        /// An object containing the city name, city code, and city ID
        public var city: City? {
          get {
            return (resultMap["city"] as? ResultMap).flatMap { City(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "city")
          }
        }

        /// The first name of the person associated with the shipping/billing address
        public var firstname: String? {
          get {
            return resultMap["firstname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstname")
          }
        }

        /// The family name of the person associated with the shipping/billing address
        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        /// An object containing the region name, region code, and region ID
        public var region: Region? {
          get {
            return (resultMap["region"] as? ResultMap).flatMap { Region(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "region")
          }
        }

        /// An array of strings that define the street number and name
        public var street: [String?]? {
          get {
            return resultMap["street"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "street")
          }
        }

        /// The customer's company
        public var company: String? {
          get {
            return resultMap["company"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "company")
          }
        }

        /// The customer's ZIP or postal code
        public var postcode: String? {
          get {
            return resultMap["postcode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postcode")
          }
        }

        /// The telephone number
        public var telephone: String? {
          get {
            return resultMap["telephone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "telephone")
          }
        }

        public struct City: GraphQLSelectionSet {
          public static let possibleTypes = ["City"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("code", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, code: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "City", "id": id, "name": name, "code": code])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var code: String? {
            get {
              return resultMap["code"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "code")
            }
          }
        }

        public struct Region: GraphQLSelectionSet {
          public static let possibleTypes = ["CustomerAddressRegion"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("region", type: .scalar(String.self)),
            GraphQLField("region_code", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(region: String? = nil, regionCode: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "CustomerAddressRegion", "region": region, "region_code": regionCode])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// The state or province name
          public var region: String? {
            get {
              return resultMap["region"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "region")
            }
          }

          /// The address region code
          public var regionCode: String? {
            get {
              return resultMap["region_code"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "region_code")
            }
          }
        }
      }
    }
  }
}

public final class RevokeTokenMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    mutation RevokeToken {
      response: revokeCustomerToken {
        __typename
        result
      }
    }
    """

  public let operationName = "RevokeToken"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("revokeCustomerToken", alias: "response", type: .object(Response.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(response: Response? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "response": response.flatMap { (value: Response) -> ResultMap in value.resultMap }])
    }

    /// Revoke the customer token
    public var response: Response? {
      get {
        return (resultMap["response"] as? ResultMap).flatMap { Response(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "response")
      }
    }

    public struct Response: GraphQLSelectionSet {
      public static let possibleTypes = ["RevokeCustomerTokenOutput"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("result", type: .nonNull(.scalar(Bool.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(result: Bool) {
        self.init(unsafeResultMap: ["__typename": "RevokeCustomerTokenOutput", "result": result])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var result: Bool {
        get {
          return resultMap["result"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "result")
        }
      }
    }
  }
}

public struct CustomerDetails: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition =
    """
    fragment customerDetails on Customer {
      __typename
      id
      firstname
      lastname
      email
    }
    """

  public static let possibleTypes = ["Customer"]

  public static let selections: [GraphQLSelection] = [
    GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
    GraphQLField("id", type: .scalar(Int.self)),
    GraphQLField("firstname", type: .scalar(String.self)),
    GraphQLField("lastname", type: .scalar(String.self)),
    GraphQLField("email", type: .scalar(String.self)),
  ]

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: Int? = nil, firstname: String? = nil, lastname: String? = nil, email: String? = nil) {
    self.init(unsafeResultMap: ["__typename": "Customer", "id": id, "firstname": firstname, "lastname": lastname, "email": email])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  /// The ID assigned to the customer
  @available(*, deprecated, message: "id is not needed as part of Customer because on server side it can be identified based on customer token used for authentication. There is no need to know customer ID on the client side.")
  public var id: Int? {
    get {
      return resultMap["id"] as? Int
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  /// The customer's first name
  public var firstname: String? {
    get {
      return resultMap["firstname"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "firstname")
    }
  }

  /// The customer's family name
  public var lastname: String? {
    get {
      return resultMap["lastname"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "lastname")
    }
  }

  /// The customer's email address. Required
  public var email: String? {
    get {
      return resultMap["email"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "email")
    }
  }
}
